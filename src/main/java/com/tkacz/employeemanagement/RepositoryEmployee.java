package com.tkacz.employeemanagement;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RepositoryEmployee extends JpaRepository<Employee, Integer> {

  public List<Employee> findByOrderByGrossSalaryAsc();
  public List<Employee> findByUser2(User2 user2);
}
