package com.tkacz.employeemanagement;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


@EnableWebSecurity
@Configuration
public class UserSpringSecurity implements UserDetailsService {

    private UserRepository userRepository;


    public UserSpringSecurity(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        final User2 user2 = userRepository.findByName(name);
        if (user2 == null) {
            throw new UsernameNotFoundException(name);
        }
        UserDetails userDetails = org.springframework.security.core.userdetails.User.withUsername(user2.getName())
                .password(user2.getPassword()).roles("USER").build();
        return userDetails;
    }

}


