package com.tkacz.employeemanagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class EmployeeController {
@Autowired
    BCryptPasswordEncoder encoder;
    @Autowired
    private ServiceEmployee serviceEmployee;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    RepositoryEmployee repositoryEmployee;

    @GetMapping("/employees")
    public String ShowEmployeesList(Model model, @RequestParam(required = false) String sort) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        final User2 user = userRepository.findByName(currentPrincipalName);
        List<Employee> listEmployees = repositoryEmployee.findByUser2(user);
        model.addAttribute("listEmployees", listEmployees);
        return "employees";
    }

    @GetMapping("/employee/new")
    public String showNewEmployee(Model model) {
        Employee employee = new Employee();
        model.addAttribute("employee", employee);
        return "new_employee";
    }

    @PostMapping("/employee/save")
    public String saveEmployee(Employee employee) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        final User2 user = userRepository.findByName(currentPrincipalName);
        employee.setUser2(user);
        serviceEmployee.save(employee);
        return "redirect:/employees";

    }

    @GetMapping("/registration")
    public String showRegistration() {
        return "registration";
    }

    @RequestMapping(value = "/saveRegistration", method = RequestMethod.POST)
    public String saveUser(@ModelAttribute User2 user2) {
        user2.setPassword(encoder.encode(user2.getPassword()));
        userRepository.save(user2);
        return "redirect:/employees";

    }
}