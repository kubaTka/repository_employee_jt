package com.tkacz.employeemanagement;

import com.fasterxml.jackson.annotation.JsonGetter;

public class EmployeeWsm {
    //WSM -web service model

    private final String name;

    public EmployeeWsm(String name) {
        this.name = name;
    }

    @JsonGetter("imie")
    public String getName() {
        return name;
    }
}

