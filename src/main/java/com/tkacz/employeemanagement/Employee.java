package com.tkacz.employeemanagement;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class Employee {
    @Id

    private UUID id=UUID.randomUUID();
    private String name;
    private String last_name;
    private String password;
    protected double grossSalary;
    protected double taxContractOfEmployment;
    protected double contractOfEmployment;
    protected double taxContractOfMandate;
    protected double contractOfMandate;
    @ManyToOne
    @JoinColumn(name = "user2_id", referencedColumnName = "id")
    User2 user2;

    public User2 getUser2() {
        return user2;
    }

    public void setUser2(User2 user2) {
        this.user2 = user2;
    }

    public Employee() {
    }

    public UUID getId() {
        return id;
    }

    public double getTaxContractOfEmployment() {
        return taxContractOfEmployment;
    }

    public String getName() {
        return name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getPassword() {
        return password;
    }

    public double getGrossSalary() {
        return grossSalary;
    }

    public double getContractOfEmployment() {
        return contractOfEmployment;
    }

    public double getTaxContractOfMandate() {
        return taxContractOfMandate;
    }

    public double getContractOfMandate() {
        return contractOfMandate;
    }


    public void setId(UUID id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setGrossSalary(double grossSalary) {
        this.grossSalary = grossSalary;
    }

    public void setTaxContractOfEmployment(double taxContractOfEmployment) {
        this.taxContractOfEmployment = taxContractOfEmployment;
    }

    public void setContractOfEmployment(double contractOfEmployment) {
        this.contractOfEmployment = contractOfEmployment;
    }

    public void setTaxContractOfMandate(double taxContractOfMandate) {
        this.taxContractOfMandate = taxContractOfMandate;
    }

    public void setContractOfMandate(double contractOfMandate) {
        this.contractOfMandate = contractOfMandate;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", password='" + password + '\'' +
                ", grossSalary=" + grossSalary +
                ", taxContractOfEmployment=" + taxContractOfEmployment +
                ", contractOfEmployment=" + contractOfEmployment +
                ", taxContractOfMandate=" + taxContractOfMandate +
                ", contractOfMandate=" + contractOfMandate +
                '}';
    }
}



