package com.tkacz.employeemanagement;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserRepository extends JpaRepository<User2, UUID> {

    public User2 findByName(String name);
}
