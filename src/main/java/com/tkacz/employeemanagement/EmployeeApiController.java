package com.tkacz.employeemanagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmployeeApiController {

    @Autowired
    ServiceEmployee serviceEmployee;
    @Autowired
    EmployeeJdbcRepository employeeJdbcRepository;

 /*   @GetMapping("/api/employees")
    public List<EmployeeWsm> getEmployees() {
        List<Employee> employees = serviceEmployee.listAll();
        return employees.stream().map(e -> new EmployeeWsm(e.getName())).collect(Collectors.toList());
    }*/

    @GetMapping("/api/employees")
    public List<EmployeeWsm> getEmployees() {

        return employeeJdbcRepository.findAll();
    }
}