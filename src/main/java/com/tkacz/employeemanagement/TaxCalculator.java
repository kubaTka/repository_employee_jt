package com.tkacz.employeemanagement;

import org.springframework.stereotype.Component;

@Component
public class TaxCalculator {

    public double calculateTaxContractOfEmployment(Employee employee) {
        return employee.grossSalary * 18 / 100;

    }

    public double calculateContractOfEmployment(Employee employee) {
        return employee.grossSalary - calculateTaxContractOfEmployment(employee);
    }

    public double calculateTaxContractOfMandate(Employee employee) {
        return employee.grossSalary * 17 / 100;
    }

    public double calculateContractOfMandate(Employee employee) {
        return employee.grossSalary - calculateTaxContractOfMandate(employee);
    }
}
