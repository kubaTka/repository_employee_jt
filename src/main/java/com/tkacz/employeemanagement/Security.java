package com.tkacz.employeemanagement;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class Security extends WebSecurityConfigurerAdapter {
    @Autowired
    private final UserSpringSecurity userSpringSecurity;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public Security(UserSpringSecurity userSpringSecurity, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userSpringSecurity = userSpringSecurity;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/employees").hasRole("USER")

                .antMatchers(HttpMethod.GET, "/registration").permitAll()
                .antMatchers(HttpMethod.POST, "/saveRegistration").permitAll()
                .anyRequest().authenticated().and().formLogin().
                permitAll()
                .and()
                .logout();
    }

    public void configure(AuthenticationManagerBuilder a) throws Exception {
        a.userDetailsService(userSpringSecurity).passwordEncoder(bCryptPasswordEncoder);
    }
}




