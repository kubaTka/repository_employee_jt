package com.tkacz.employeemanagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

@Component
public class EmployeeJdbcRepository {
    @Autowired
    private DataSource dataSource;

    public List<EmployeeWsm> findAll() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("Select name from employee", (rs, i) -> new EmployeeWsm("x"+rs.getString("name")));
    }
}


