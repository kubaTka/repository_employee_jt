package com.tkacz.employeemanagement;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class ServiceEmployee {
    @Autowired
    private RepositoryEmployee repositoryEmployee;
    @Autowired
    private TaxCalculator taxCalculator;

    public List<Employee> listAll(String sort) {
        if(sort==null){
           return repositoryEmployee.findAll();
        }else {
            return repositoryEmployee.findByOrderByGrossSalaryAsc();
        }
        // return (List<Employee>) repositoryEmployee.findAll(Sort.by(Sort.Direction.DESC, "name"));
    }

    public void save(Employee employee) {
        employee.setTaxContractOfEmployment(taxCalculator.calculateTaxContractOfEmployment(employee));
        employee.setContractOfEmployment(taxCalculator.calculateContractOfEmployment(employee));
        employee.setTaxContractOfMandate(taxCalculator.calculateTaxContractOfMandate(employee));
        employee.setContractOfMandate(taxCalculator.calculateContractOfMandate(employee));

        repositoryEmployee.save(employee);

    }


}
