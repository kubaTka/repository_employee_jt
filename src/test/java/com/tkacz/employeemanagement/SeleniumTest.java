package com.tkacz.employeemanagement;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

public class SeleniumTest {
    WebDriver driver;


    @BeforeTest
    public void setup() {

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);
        driver.get("http://localhost:8080/login");
    }

    @Test(groups = {"chrome"})
    public void loginUser() {
        setup();
        WebElement webElement = driver.findElement(By.id("username"));
        webElement.click();
        webElement.sendKeys("Zuza");



    }

    @Test(groups = {"edge"})
    public void loginPassword() {
        setup();
        WebElement webElement = driver.findElement(By.id("password"));
        webElement.click();
        webElement.sendKeys("zuza");

    }

    @AfterMethod
    public void cleanUp() {
        driver.quit();

    }
}
